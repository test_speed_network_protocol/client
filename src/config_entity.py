from pydantic_settings import BaseSettings, SettingsConfigDict


class MyBaseSettings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8', extra="ignore")


class ServerSettings(MyBaseSettings):
    model_config = SettingsConfigDict(env_prefix="TEST_SERVER_")

    HOST: str = 'localhost'
    HOST_GRPC: str = 'localhost'
    PORT: int
    PORT_GRPC: int = 50051


class TestRunSettings(MyBaseSettings):
    model_config = SettingsConfigDict(env_prefix="TEST_RUN_")

    LIST_PROTOCOL: str
    COUNT_ITERATION: int = 3
    LIST_BATCH_SIZE: str
    LIST_GET_COUNT_ELEMENT: str
    MAX_CONCURRENT_TASKS: int = 10

    def get_list_protocol(self) -> list[str]:
        return [x for x in self.LIST_PROTOCOL.split(';') if x != '']

    def get_list_batch_size(self) -> list[int]:
        return [int(x) for x in self.LIST_BATCH_SIZE.split(';') if x != '']

    def get_list_get_count_element(self) -> list[int]:
        return [int(x) for x in self.LIST_GET_COUNT_ELEMENT.split(';') if x != '']
