import aiohttp
from config_entity import ServerSettings
from protocols.base_protocol import BaseProtocol
import configs
import umsgpack


class ProtocolMsgPack(BaseProtocol):
    name = 'msgpack'

    def __init__(self, config: ServerSettings) -> None:
        super().__init__()
        self._session: aiohttp.ClientSession | None = None
        self._url = f'http://{config.HOST}:{config.PORT}/get_data/msgpack'

    async def process_response(self, response, index: int, batch_size: int) -> bool:
        binary_data = await response.read()
        text = umsgpack.unpackb(binary_data)
        response_json = self.convert_text_to_json(text_input=text)
        if not self.response_is_correct(index=index, batch_size=batch_size, response=response_json):
            self._result_repo.add_error(self.name)


protocol_msg_pack: ProtocolMsgPack | None = None


def get_protocol_msgpack() -> ProtocolMsgPack:
    global protocol_msg_pack
    if protocol_msg_pack is None:
        protocol_msg_pack = ProtocolMsgPack(config=configs.server_settings)

    return protocol_msg_pack
