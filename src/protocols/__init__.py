from asyncio import BaseProtocol

from protocols import protocol_json_gzip, protocol_pure_json, protocol_avro, protocol_grpc, protocol_msgpack


dict_of_protocol: dict[str, BaseProtocol] = {
    protocol_json_gzip.get_protocol_json_gzip().name: protocol_json_gzip.get_protocol_json_gzip(),
    protocol_pure_json.get_protocol_pure_json().name: protocol_pure_json.get_protocol_pure_json(),
    protocol_avro.get_protocol_avro().name: protocol_avro.get_protocol_avro(),
    protocol_msgpack.get_protocol_msgpack().name: protocol_msgpack.get_protocol_msgpack(),
    protocol_grpc.get_protocol_grpc().name: protocol_grpc.get_protocol_grpc(),
}


async def close_resource() -> None:
    for protocol in dict_of_protocol.values():
        await protocol.close()
