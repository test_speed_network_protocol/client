import aiohttp
import orjson
from google.protobuf.json_format import MessageToDict
from grpc import aio

import configs
from config_entity import ServerSettings
from protocols.base_protocol import BaseProtocol
from protocols.grpc_test import test_pb2, test_pb2_grpc


class ProtocolGRPC(BaseProtocol):
    name = 'grpc'

    def __init__(self, config: ServerSettings) -> None:
        super().__init__()
        self._server_url = f'{config.HOST_GRPC}:{config.PORT_GRPC}'
        self._channel = None
        self._stub = None

    async def get_session(self) -> aiohttp.ClientSession:
        if self._channel is None:
            MAX_MESSAGE_LENGTH = -1
            self._channel = aio.insecure_channel(
                self._server_url,
                options=[
                    ('grpc.max_send_message_length', MAX_MESSAGE_LENGTH),
                    ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH),
                ],
            )
            self._stub = test_pb2_grpc.DataServiceStub(self._channel)

        return self._stub

    async def get_data(self, index: int, batch_size: int):
        start_index = index * batch_size
        text = 'Ответ еще не получили'
        try:
            # Создание клиента для сервиса DataService
            stub = await self.get_session()
            request = test_pb2.DataRequest(start_index=start_index, batch_size=batch_size)
            response = await stub.GetData(request)
            response_json = MessageToDict(
                response,
                preserving_proto_field_name=True,
                use_integers_for_enums=False,
                including_default_value_fields=True,
            )
            if not self.response_is_correct(index=index, batch_size=batch_size, response=response_json):
                self._result_repo.add_error(self.name)
        except Exception as exc_info:
            self._result_repo.add_error(self.name)
            print(f'Задача "{index+1}" - ошибка: "{exc_info}" text {text}')

    async def process_response(self, response, index: int, batch_size: int) -> bool:
        # в данной реализации не используется
        pass

    async def close(self) -> None:
        if self._channel is not None:
            await self._channel.close()


protocol_grpc: ProtocolGRPC | None = None


def get_protocol_grpc() -> ProtocolGRPC:
    global protocol_grpc
    if protocol_grpc is None:
        protocol_grpc = ProtocolGRPC(config=configs.server_settings)

    return protocol_grpc
