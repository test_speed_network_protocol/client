from io import BytesIO
import aiohttp
import fastavro
from config_entity import ServerSettings
from protocols.base_protocol import BaseProtocol
import configs


class ProtocolAVRO(BaseProtocol):
    name = 'avro'
    avro_schema = {
        "type": "record",
        "name": "MyData",
        "fields": [
            {
                "name": "meta",
                "type": {
                    "type": "record",
                    "name": "Meta",
                    "fields": [
                        {"name": "start_index_next_page", "type": "int"},
                        {"name": "control_value", "type": "string"},
                    ],
                },
            },
            {
                "name": "data",
                "type": {
                    "type": "array",
                    "items": {
                        "type": "record",
                        "name": "DataItem",
                        "fields": [{"name": "index", "type": "int"}, {"name": "data", "type": "string"}],
                    },
                },
            },
        ],
    }

    def __init__(self, config: ServerSettings) -> None:
        super().__init__()
        self._session: aiohttp.ClientSession | None = None
        self._url = f'http://{config.HOST}:{config.PORT}/get_data/avro'

    async def get_session(self) -> aiohttp.ClientSession:
        if self._session is None:
            self._session = aiohttp.ClientSession()

        return self._session

    async def process_response(self, response, index: int, batch_size: int) -> bool:
        content = await response.read()
        reader = fastavro.reader(BytesIO(content), reader_schema=self.avro_schema)
        records = [record for record in reader]
        if len(records) == 0:
            self._result_repo.add_error(self.name)
        response_json = records[0]
        if not self.response_is_correct(index=index, batch_size=batch_size, response=response_json):
            self._result_repo.add_error(self.name)


protocol_avro: ProtocolAVRO | None = None


def get_protocol_avro() -> ProtocolAVRO:
    global protocol_avro
    if protocol_avro is None:
        protocol_avro = ProtocolAVRO(config=configs.server_settings)

    return protocol_avro
