from abc import ABC
import abc
from http import HTTPStatus
from typing import Any

import aiohttp
import orjson

from repository.result_repository import ResultRepository, get_result_repository


class BaseProtocol(ABC):
    name: str = 'Не УКАЗАН'

    def __init__(self) -> None:
        super().__init__()
        self._result_repo: ResultRepository = get_result_repository()

    async def get_session(self) -> aiohttp.ClientSession:
        if self._session is None:
            self._session = aiohttp.ClientSession()

        return self._session

    async def get_data(self, index: int, batch_size: int):
        params = {'start_index': index * batch_size, 'batch_size': batch_size}
        # print(params)
        session = await self.get_session()
        text = 'Ответ еще не получили'
        try:
            async with session.get(self._url, params=params) as response:
                if response.status != HTTPStatus.OK:
                    self._result_repo.add_error(self.name)
                    return
                await self.process_response(response=response, index=index, batch_size=batch_size)
        except aiohttp.ClientConnectionError as e:
            self._result_repo.add_error(self.name)
            print(f'Задача "{index+1}" - Connection error: "{e}"')
        except Exception as exc_info:
            self._result_repo.add_error(self.name)
            print(f'Задача "{index+1}" - ошибка: "{exc_info}" text {text}')

    @abc.abstractmethod
    async def process_response(self, response, index: int, batch_size: int) -> bool:
        pass

    async def close(self) -> None:
        if self._session is not None:
            await self._session.close()

    def convert_text_to_json(self, text_input: str) -> dict[Any, Any]:
        return orjson.loads(text_input)

    def response_is_correct(self, index: int, batch_size: int, response: dict[str, Any]) -> bool:
        etalon_string = f'Порция данных {index*batch_size}'
        data_from_response = response.get('meta', {}).get('control_value', '')

        return etalon_string == data_from_response
