import asyncio
from datetime import datetime
import random
import configs
from protocols.base_protocol import BaseProtocol
from protocols.protocol_json_gzip import get_protocol_json_gzip
from repository.result_repository import get_result_repository
from protocols import close_resource, dict_of_protocol


async def task_get_data(semaphore: asyncio.Semaphore, protocol: BaseProtocol, batch_size: int, index: int):
    print(f'Задача {index+1} запущена')
    async with semaphore:
        print(f'Задача {index+1} получили семафор')
        # await asyncio.sleep(1)
        await protocol.get_data(index, batch_size)
    print(f'Задача {index+1} завершена')


async def run_tasks_for_test(protocol: BaseProtocol, batch_size: int, count_element: int):
    date_start = datetime.now()
    count_tasks = count_element // batch_size
    semaphore = asyncio.Semaphore(configs.run_test_settings.MAX_CONCURRENT_TASKS)

    list_of_tasks = [task_get_data(semaphore, protocol, batch_size, index) for index in range(count_tasks)]
    print(f'Создали {len(list_of_tasks)} задач')
    await asyncio.gather(*list_of_tasks)

    date_finish = datetime.now()
    seconds_to_work = (date_finish - date_start).total_seconds()

    result_repository = get_result_repository()
    result_repository.add_result(protocol.name, seconds_to_work)


async def run_test():
    result_repository = get_result_repository()
    result_repository.clear_result()
    try:
        for batch in configs.run_test_settings.get_list_batch_size():
            for count_element in configs.run_test_settings.get_list_get_count_element():
                result_repository.start_new_series_test(
                    configs.run_test_settings.get_list_protocol(), batch, count_element
                )
                for i in range(configs.run_test_settings.COUNT_ITERATION):
                    protocol_list = configs.run_test_settings.get_list_protocol()
                    random.shuffle(protocol_list)

                    for protocol in protocol_list:
                        print(
                            f'batch "{batch}" - count_element "{count_element}" - protocol "{protocol}" - iteration "{i+1}"'
                        )
                        protocol_instance = dict_of_protocol.get(protocol)
                        if protocol_instance is None:
                            raise ValueError(f'Не удалось найти протокол "{protocol}"')

                        await run_tasks_for_test(protocol_instance, batch, count_element)
        result_repository.save_result()
    except Exception as exc_info:
        print(exc_info)
    await close_resource()


async def tets():
    protocol_instance = get_protocol_json_gzip()
    await protocol_instance.get_data(1, 1000)


if __name__ == "__main__":
    # asyncio.run(tets())
    asyncio.run(run_test())
