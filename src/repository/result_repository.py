from datetime import datetime
import pathlib
from typing import Any
import configs
import csv


class ResultRepository:
    def __init__(self, base_path: pathlib.Path):
        self._dict_result: dict[str, Any] = {}
        self._curr_key: str = ''
        self._base_path = base_path
        self._count_measurements = 0

    def clear_result(self):
        self._dict_result: dict[str, Any] = {}

    def start_new_series_test(self, protocols: list[str], batch_size: int, count_element: int) -> None:
        self._curr_key = f'{batch_size}_{count_element}'
        protocols: dict[str, list[int]] = {protocol: list() for protocol in protocols}
        errors: dict[str, int] = {protocol: 0 for protocol in protocols}
        self._dict_result[self._curr_key] = {
            'protocols': protocols,
            'errors': errors,
            'params': {'batch_size': batch_size, 'count_element': count_element},
        }
        self._count_measurements = 0

    def add_result(self, protocol: str, seconds_to_work: int) -> None:
        self._dict_result[self._curr_key]['protocols'][protocol].append(seconds_to_work)
        self._count_measurements = len(self._dict_result[self._curr_key]['protocols'][protocol])

    def add_error(self, protocol: str) -> None:
        self._dict_result[self._curr_key]['errors'][protocol] = (
            self._dict_result[self._curr_key]['errors'][protocol] + 1
        )

    def save_result(self) -> None:
        current_datetime = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_path = self._base_path / f'result_{current_datetime}.csv'
        list_column = [f'Измерение - {i+1} (sec.)' for i in range(self._count_measurements)]
        with open(file_path, 'w', newline='') as file:
            csv_writer = csv.writer(file)
            for value in self._dict_result.values():
                csv_writer.writerow([' '])
                csv_writer.writerow([' '])
                csv_writer.writerow(
                    [
                        'Запись результатов для параметров:',
                        f'batch_size: {value["params"]["batch_size"]}, count_element: {value["params"]["count_element"]}',
                    ]
                )
                csv_writer.writerow([' '])
                csv_writer.writerow(['Имя протокола', *list_column, 'Среднее значение'])
                for protocol, list_result in value['protocols'].items():
                    mean_value = sum(list_result) / len(list_result) if len(list_result) > 0 else 0
                    csv_writer.writerow([protocol, *list_result, mean_value])

                csv_writer.writerow([' '])
                csv_writer.writerow(['=========ERRORS============================'])
                csv_writer.writerow(['Имя протокола', 'Количество ошибок'])
                for protocol, count_error in value['errors'].items():
                    csv_writer.writerow([protocol, count_error])


result_repository: ResultRepository | None = None


def get_result_repository() -> ResultRepository:
    global result_repository
    if result_repository is None:
        result_repository = ResultRepository(base_path=configs.path_to_result)

    return result_repository
