import pathlib

from config_entity import ServerSettings, TestRunSettings

_base_to_env = pathlib.Path(__file__).parents[1] / 'dev_dc_tst' / '.env'
if _base_to_env.exists():
    from dotenv import load_dotenv

    load_dotenv(_base_to_env, override=True)

server_settings = ServerSettings()
run_test_settings = TestRunSettings()
print(server_settings)
print(run_test_settings)
path_to_result = pathlib.Path(__file__).parents[1] / 'result_tests'
